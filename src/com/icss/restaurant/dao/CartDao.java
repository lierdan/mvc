package com.icss.restaurant.dao;

import com.icss.restaurant.entity.Cartvo;

import java.util.List;

public interface CartDao {
    public List<Cartvo> findAllCartByUid(int uid);

    public int deleteCart(String  strcid)throws Exception;
}
