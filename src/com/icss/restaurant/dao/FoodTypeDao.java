package com.icss.restaurant.dao;

import com.icss.restaurant.entity.FoodType;

import java.util.List;

/**
 * 菜系类
 */
public interface FoodTypeDao {
    public  int addFoodType(String foodTypeName);
    public  int deleteFoodType(int ftno);
    public  int updateFoodTypeName(FoodType foodType);
    public List<FoodType> findAllFoodType();
}
