package com.icss.restaurant.dao;

import com.icss.restaurant.entity.OrderDetail;

public interface OrderDetailDao {
    public int addOrdersDetail(OrderDetail orderDetail) throws Exception;
}
