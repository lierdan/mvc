package com.icss.restaurant.dao;

import com.icss.restaurant.entity.Menu;

import java.util.List;

/**
 * private int mid;//菜单名
 * private String mname;//菜名
 * private  double mprice;//菜价格
 * private  String mintroduce;//菜简介
 * private  String mimg;//菜图片
 * private  int ftno;//所属菜系
 */

public interface MenuDao {
    public int addMenu(Menu menu);

    public int deleteMenu(int mid);

    public int updateMenu(Menu menu);

    public List<Menu> findAllMenu();

    public  Menu findById(int mid)throws Exception;

    //查询记录数
    //查询分页数据
    public int findMenuCount() throws Exception;

    public List<Menu> findAllMenuByPage(int currentPage, int pazeSize) throws Exception;
}
