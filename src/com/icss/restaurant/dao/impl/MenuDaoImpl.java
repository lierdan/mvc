package com.icss.restaurant.dao.impl;

import com.icss.restaurant.dao.MenuDao;
import com.icss.restaurant.entity.Menu;
import com.icss.restaurant.util.DBFactory;
import org.junit.Test;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

//private int mid;//菜单名
//private String mname;//菜名
//private  double mprice;//菜价格
//private  String mintroduce;//菜简介
//private  String mimg;//菜图片
//private  int ftno;//所属菜系
public class MenuDaoImpl implements MenuDao {
    @Override
    public int addMenu(Menu menu) {
        int row = 0;
        String mname = menu.getMname();
        double mprice = menu.getMprice();
        String mintroduce = menu.getMintroduce();
        String mimg = menu.getMimg();
        int ftno = menu.getFtno();
        int mid = menu.getMid();

        Connection connection = null;
        PreparedStatement preparedStatement = null;

        try {
            connection = DBFactory.openConnection();
            String sql = "insert into menu (mname,mprice,mintroduce,mimg,ftno) values (?,?,?,?,?)";
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, mname);
            preparedStatement.setDouble(2, mprice);
            preparedStatement.setString(3, mintroduce);
            preparedStatement.setString(4, mimg);
            preparedStatement.setInt(5, ftno);
            row = preparedStatement.executeUpdate();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (preparedStatement != null) {
                try {
                    preparedStatement.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }

        return row;
    }


    @Override
    public int deleteMenu(int mid) {
        int row = 0;

        Connection connection = null;
        PreparedStatement preparedStatement = null;

        try {
            connection = DBFactory.openConnection();
            String sql = "delete from menu where mid =?";
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, mid);
            row = preparedStatement.executeUpdate();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (preparedStatement != null) {
                try {
                    preparedStatement.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }

        return row;
    }

    @Override
    public int updateMenu(Menu menu) {
        int row = 0;
        String mname = menu.getMname();
        double mprice = menu.getMprice();
        String mintroduce = menu.getMintroduce();
        String mimg = menu.getMimg();
        int ftno = menu.getFtno();
        int mid = menu.getMid();

        Connection connection = null;
        PreparedStatement preparedStatement = null;

        try {
            connection = DBFactory.openConnection();
            String sql = " update  menu set mname=?,mprice=?,mintroduce=?,mimg=?,ftno=? where mid=?";
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, mname);
            preparedStatement.setDouble(2, mprice);
            preparedStatement.setString(3, mintroduce);
            preparedStatement.setString(4, mimg);
            preparedStatement.setInt(5, ftno);
            preparedStatement.setInt(6, mid);
            row = preparedStatement.executeUpdate();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (preparedStatement != null) {
                try {
                    preparedStatement.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }

        return row;
    }

    @Override
    public List<Menu> findAllMenu() {
        List<Menu> list = new ArrayList<>();

        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try {
            connection = DBFactory.openConnection();
            String sql = "select * from menu";
            preparedStatement = connection.prepareStatement(sql);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                Menu menu=new Menu();
                menu.setMid(resultSet.getInt("mid"));
                menu.setMprice(resultSet.getDouble("mprice"));
                menu.setMname(resultSet.getString("mname"));
                menu.setMintroduce(resultSet.getString("mintroduce"));
                menu.setMimg(resultSet.getString("mimg"));
                menu.setFtno(resultSet.getInt("ftno"));
                list.add(menu);
            }

        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (preparedStatement!=null) {
                try {
                    preparedStatement.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        return list;
    }

    @Override
    public Menu findById(int mid) throws Exception {
            Menu menu=null;

        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try {
            connection = DBFactory.openConnection();
            String sql = "select * from menu where mid=?";
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1,mid);
            resultSet = preparedStatement.executeQuery();
            if(resultSet.first()) {
                 menu=new Menu();
                menu.setMid(resultSet.getInt("mid"));
                menu.setMprice(resultSet.getDouble("mprice"));
                menu.setMname(resultSet.getString("mname"));
                menu.setMintroduce(resultSet.getString("mintroduce"));
                menu.setMimg(resultSet.getString("mimg"));
                menu.setFtno(resultSet.getInt("ftno"));

            }

        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (preparedStatement!=null) {
                try {
                    preparedStatement.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }

            return menu;
    }

    @Override
    public int findMenuCount() throws Exception {
return 0;
    }

    @Override
    public List<Menu> findAllMenuByPage(int currentPage,int pazeSize) throws Exception {
        List<Menu> list = new ArrayList<>();

        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try {
            connection = DBFactory.openConnection();
            String sql = "select * from menu limit ?,?";
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1,(currentPage-1)*pazeSize);
            preparedStatement.setInt(2,pazeSize);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                Menu menu=new Menu();
                menu.setMid(resultSet.getInt("mid"));
                menu.setMprice(resultSet.getDouble("mprice"));
                menu.setMname(resultSet.getString("mname"));
                menu.setMintroduce(resultSet.getString("mintroduce"));
                menu.setMimg(resultSet.getString("mimg"));
                menu.setFtno(resultSet.getInt("ftno"));
                list.add(menu);
            }

        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (preparedStatement!=null) {
                try {
                    preparedStatement.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        return list;
    }

    public static void main(String[] args) throws Exception {
        System.out.println(new MenuDaoImpl().findById(1));
    }
}
