package com.icss.restaurant.dao.impl;

import com.icss.restaurant.dao.FoodTypeDao;
import com.icss.restaurant.entity.FoodType;
import com.icss.restaurant.util.DBFactory;
import org.junit.Test;

import java.sql.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

/**
 * dao实现类
 */
public class FoodTypeDaoImpl implements FoodTypeDao {
    @Override
    public int addFoodType(String foodTypeName) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        int row = 0;
        try {
            connection = DBFactory.openConnection();
            String sql = "insert into food_type (ftname) values(?);";
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, foodTypeName);
            row = preparedStatement.executeUpdate();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (preparedStatement != null) {
                try {
                    preparedStatement.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }

        return row;
    }


    @Override
    public int deleteFoodType(int ftno) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        int row = 0;
        try {
            connection = DBFactory.openConnection();
            String sql = "delete from food_type where ftno = ? ";
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, ftno);
            row = preparedStatement.executeUpdate();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (preparedStatement != null) {
                try {
                    preparedStatement.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }


        return row;
    }

    @Override
    public int updateFoodTypeName(FoodType foodType) {
        int row = 0;
        String ftname=foodType.getFtname();
        int ftno=foodType.getFtno();
        Connection connection = null;
        PreparedStatement preparedStatement = null;

        try {
            connection = DBFactory.openConnection();
            String sql="update food_type set ftname=? where ftno=?";
            preparedStatement=connection.prepareStatement(sql);
            preparedStatement.setString(1,ftname);
            preparedStatement.setInt(2,ftno);
            row=preparedStatement.executeUpdate();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (preparedStatement!=null) {
                try {
                    preparedStatement.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }

        return row;
    }



    @Override
    public List<FoodType> findAllFoodType() {
        List<FoodType> list=new ArrayList<>();

        Connection connection=null;
        PreparedStatement preparedStatement=null;
        ResultSet resultSet=null;
        try {
            connection=DBFactory.openConnection();
            String sql="select * from food_type";
            preparedStatement=connection.prepareStatement(sql);
             resultSet=preparedStatement.executeQuery();
            while (resultSet.next()){
                FoodType foodType=new FoodType();
                foodType.setFtno(resultSet.getInt("ftno"));
                foodType.setFtname(resultSet.getString("ftname"));
                list.add(foodType);
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (preparedStatement!=null) {
                try {
                    preparedStatement.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }

        return list;
    }




}
