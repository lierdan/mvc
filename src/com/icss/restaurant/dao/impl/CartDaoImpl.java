package com.icss.restaurant.dao.impl;

import com.icss.restaurant.dao.CartDao;
import com.icss.restaurant.entity.Cart;
import com.icss.restaurant.entity.Cartvo;
import com.icss.restaurant.util.DBFactory;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class CartDaoImpl implements CartDao {
    @Override
    public List<Cartvo> findAllCartByUid(int uid) {
        List<Cartvo> list = new ArrayList<>();
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            connection = DBFactory.openConnection();
            String sql = "select m.*,c.count from menu m" +
                    "inner join cart c on c.mid=m.mid" +
                    "where c.uid=?";
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, uid);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                Cartvo cartvo = new Cartvo();
                cartvo.setCount(resultSet.getInt("count"));
                cartvo.setMprice(resultSet.getDouble("mprice"));
                cartvo.setMname(resultSet.getString("mname"));
                list.add(cartvo);
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (preparedStatement != null) {
                try {
                    preparedStatement.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        return null;
    }

    @Override
    public int deleteCart(String strcid) throws Exception {
        int row = 0;
        try {
            Connection connection = DBFactory.openConnection();
            String sql = "delete from care where cid=" + strcid;
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            row = preparedStatement.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return row;
    }
}
