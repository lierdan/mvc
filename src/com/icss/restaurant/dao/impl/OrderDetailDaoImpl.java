package com.icss.restaurant.dao.impl;

import com.icss.restaurant.dao.OrderDetailDao;
import com.icss.restaurant.entity.OrderDetail;
import com.icss.restaurant.util.DBFactory;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class OrderDetailDaoImpl implements OrderDetailDao {
    @Override
    public int addOrdersDetail(OrderDetail orderDetail) throws SQLException {
        int row = 0;
        Connection connection = null;
        PreparedStatement preparedStatement = null;


        try {
            connection = DBFactory.openConnection();
            String sql = "insert into orders_detail(mid,oid,odcount,price)values (?,?,?,?)";
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, orderDetail.getMid());
            preparedStatement.setString(2, orderDetail.getOid());
            preparedStatement.setInt(3, orderDetail.getOdcount());
            preparedStatement.setDouble(4, orderDetail.getPrice());
            row = preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }

            return row;
        }
    }
}
