package com.icss.restaurant.dao.impl;

import com.icss.restaurant.dao.OrdersDao;
import com.icss.restaurant.entity.Orders;
import com.icss.restaurant.util.DBFactory;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class OrdersDaoImpl implements OrdersDao {
    @Override
    public int addOrders(Orders orders) throws Exception {
        int row = 0;
        Connection connection = null;
        PreparedStatement preparedStatement = null;

        connection = DBFactory.openConnection();
        String sql = "insert  into orders (oid,ordertime,uid,uremark,state,total)values (?,?,?,\'?\',?,?)";
        try {
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1,orders.getOid());
            preparedStatement.setTimestamp(2,orders.getOrderTime());
            preparedStatement.setInt(3,orders.getUid());
            preparedStatement.setString(4,orders.getUremark());
            preparedStatement.setString(5,orders.getState());
            preparedStatement.setDouble(6,orders.getTotal());
            row = preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            if (preparedStatement!=null) {
                preparedStatement.close();
            }
        }




        return row;
    }
}
