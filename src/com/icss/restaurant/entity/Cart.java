package com.icss.restaurant.entity;

/**
 * 购物车
 */
public class Cart {
    private int cid;//购物车编号
    private int mid;//菜单id
    private  int uid;//用户id
    private  int count;//数量

    public Cart() {
    }

    @Override
    public String toString() {
        return "Cart{" +
                "cid=" + cid +
                ", mid=" + mid +
                ", uid=" + uid +
                ", count=" + count +
                '}';
    }

    public int getCid() {
        return cid;
    }

    public void setCid(int cid) {
        this.cid = cid;
    }

    public int getMid() {
        return mid;
    }

    public void setMid(int mid) {
        this.mid = mid;
    }

    public int getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }
}
