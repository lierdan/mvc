package com.icss.restaurant.entity;

public class OrderDetail {
    private int odid;//订单详情编号
    private int mid;//菜品编号
    private int odcount;//餐品数量
    private String oid;//定点标号
    private double price;//价格

    @Override
    public String toString() {
        return "OrderDetail{" +
                "odid=" + odid +
                ", mid=" + mid +
                ", odcount=" + odcount +
                ", oid='" + oid + '\'' +
                ", price=" + price +
                '}';
    }

    public OrderDetail() {
    }

    public OrderDetail(int mid, int odcount, String oid, double price) {
        this.mid = mid;
        this.odcount = odcount;
        this.oid = oid;
        this.price = price;
    }

    public int getOdid() {
        return odid;
    }

    public void setOdid(int odid) {
        this.odid = odid;
    }

    public int getMid() {
        return mid;
    }

    public void setMid(int mid) {
        this.mid = mid;
    }

    public int getOdcount() {
        return odcount;
    }

    public void setOdcount(int odcount) {
        this.odcount = odcount;
    }

    public String getOid() {
        return oid;
    }

    public void setOid(String oid) {
        this.oid = oid;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }
}