package com.icss.restaurant.entity;

public class Admin {
    private int aid;
    private String anme;
    private String apwd;
    private String aemail;
    private String aphone;

    @Override
    public String toString() {
        return "Admin{" +
                "aid=" + aid +
                ", anme='" + anme + '\'' +
                ", apwd='" + apwd + '\'' +
                ", aemail='" + aemail + '\'' +
                ", aphone='" + aphone + '\'' +
                '}';
    }

    public int getAid() {
        return aid;
    }

    public void setAid(int aid) {
        this.aid = aid;
    }

    public String getAnme() {
        return anme;
    }

    public void setAnme(String anme) {
        this.anme = anme;
    }

    public String getApwd() {
        return apwd;
    }

    public void setApwd(String apwd) {
        this.apwd = apwd;
    }

    public String getAemail() {
        return aemail;
    }

    public void setAemail(String aemail) {
        this.aemail = aemail;
    }

    public String getAphone() {
        return aphone;
    }

    public void setAphone(String aphone) {
        this.aphone = aphone;
    }

    public Admin() {
    }
}
