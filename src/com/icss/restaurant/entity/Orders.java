package com.icss.restaurant.entity;


import java.sql.Timestamp;
import java.util.Date;

public class Orders {
    private String oid;//订单号
    private Timestamp orderTime;//下单时间
    private int uid;//用户名
    private String uremark;//用户备注
    private String state;//订单状态
    private double total;//总价

    public Orders(Timestamp orderTime, int uid, String uremark, String state, double total) {
        this.orderTime = orderTime;
        this.uid = uid;
        this.uremark = uremark;
        this.state = state;
        this.total = total;
    }

    public Orders() {

    }

    @Override
    public String toString() {
        return "Orders{" +
                "oid='" + oid + '\'' +
                ", orderTime=" + orderTime +
                ", uid=" + uid +
                ", uremark='" + uremark + '\'' +
                ", state='" + state + '\'' +
                ", total=" + total +
                '}';
    }

    public String getOid() {
        return oid;
    }

    public void setOid(String oid) {
        this.oid = oid;
    }

    public Timestamp getOrderTime() {
        return orderTime;
    }

    public void setOrderTime(Timestamp orderTime) {
        this.orderTime = orderTime;
    }

    public int getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    public String getUremark() {
        return uremark;
    }

    public void setUremark(String uremark) {
        this.uremark = uremark;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }
}
