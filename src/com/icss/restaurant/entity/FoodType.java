package com.icss.restaurant.entity;

import org.junit.Test;

/**
 * 菜系实体类
 */
public class FoodType {
    private int ftno;//菜系编号
    private String ftname;//菜系名

    @Override
    public String toString() {
        return
                "菜系id：" + ftno +
                        "菜系名：" + ftname;
    }

    public int getFtno() {
        return ftno;
    }

    public void setFtno(int ftno) {
        this.ftno = ftno;
    }

    public String getFtname() {
        return ftname;
    }

    public void setFtname(String ftname) {
        this.ftname = ftname;
    }
}
