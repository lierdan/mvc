package com.icss.restaurant.entity;

import org.junit.Test;

/**
 * 菜单类
 */
public class Menu {
    private int mid;//菜单名
    private String mname;//菜名
    private  double mprice;//菜价格
    private  String mintroduce;//菜简介
    private  String mimg;//菜图片
    private  int ftno;//所属菜系

    public int getMid() {
        return mid;
    }

    public void setMid(int mid) {
        this.mid = mid;
    }

    public String getMname() {
        return mname;
    }

    public void setMname(String mname) {
        this.mname = mname;
    }

    public double getMprice() {
        return mprice;
    }

    public void setMprice(double mprice) {
        this.mprice = mprice;
    }

    public String getMintroduce() {
        return mintroduce;
    }

    public void setMintroduce(String mintroduce) {
        this.mintroduce = mintroduce;
    }

    public String getMimg() {
        return mimg;
    }

    public void setMimg(String mimg) {
        this.mimg = mimg;
    }

    public int getFtno() {
        return ftno;
    }

    public void setFtno(int ftno) {
        this.ftno = ftno;
    }

    @Override
    public String toString() {
        return
                "该菜id:" + mid +
                ", 名字:'" + mname + '\'' +
                ", 价格：" + mprice +
                ", 简介：" + mintroduce + '\'' +
                ", 图片链接：" + mimg + '\'' +
                ", 所属菜系id：" + ftno;
    }

}
