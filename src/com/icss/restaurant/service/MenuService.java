package com.icss.restaurant.service;

import com.icss.restaurant.entity.Menu;
import com.icss.restaurant.util.PageUtil;

import java.util.List;

public interface MenuService {
    public int addMenu(Menu menu);
    public int deleteMenu(int mid);
    public int updateMenu(Menu menu);
    public List<Menu> findAllMenu();

    public int findMenuCount()throws Exception;
    public PageUtil<Menu> findAllMenuByPage(int currentPage,int PageSize)throws Exception;
    public  Menu findById(int mid)throws Exception;
}
