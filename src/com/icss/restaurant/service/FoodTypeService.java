package com.icss.restaurant.service;

import com.icss.restaurant.entity.FoodType;

import java.util.List;

/**
 * 菜系service
 */
public interface FoodTypeService {
    public  int addFoodType(String foodTypeName);
    public  int deleteFoodType(int ftno);
    public  int updateFoodTypeName(FoodType foodType);
    public List<FoodType> findAllFoodType();
}
