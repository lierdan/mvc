package com.icss.restaurant.service.impl;

import com.icss.restaurant.dao.CartDao;
import com.icss.restaurant.dao.OrderDetailDao;
import com.icss.restaurant.dao.OrdersDao;
import com.icss.restaurant.dao.impl.CartDaoImpl;
import com.icss.restaurant.dao.impl.OrderDetailDaoImpl;
import com.icss.restaurant.dao.impl.OrdersDaoImpl;
import com.icss.restaurant.entity.Cartvo;
import com.icss.restaurant.entity.OrderDetail;
import com.icss.restaurant.entity.Orders;
import com.icss.restaurant.service.OrdersService;
import com.icss.restaurant.util.DBFactory;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.UUID;

public class OrdersServiceImpl implements OrdersService {
    @Override
    public void addOrders(String strcid, double total, int uid, String uremark) throws Exception {
        CartDao cartDao=new CartDaoImpl();
        OrdersDao ordersDao=new OrdersDaoImpl();
        OrderDetailDao orderDetailDao=new OrderDetailDaoImpl();
        try {
            //開啓事務
            DBFactory.beginTransaction();
            //調用數據層的發放
            cartDao.deleteCart(strcid);
            Orders orders=new Orders();
            String oid= UUID.randomUUID().toString();
            orders.setOid(oid);
            orders.setOrderTime(new Timestamp(System.currentTimeMillis()));
            orders.setState("已提交");
            orders.setTotal(total);
            orders.setUid(uid);
            orders.setUremark(uremark);
            ordersDao.addOrders(orders);
            //订单详情
            OrderDetail orderDetail= new OrderDetail();
//            orderDetail.setMid(mid);
//            orderDetail.setOdcount(odcount);
//            orderDetail.setOid(oid);
//            orderDetail.setPrice(price);
//            orderDetailDao.addOrdersDetail();
            //事務后提交
            DBFactory.commit();
        } catch (Exception e) {
            e.printStackTrace();
            //事務回滾
            DBFactory.rollback();
        }finally {
            DBFactory.closeConnection();
        }
        return ;
    }

    public Cartvo findCartByCid(int cid )throws Exception {
        Cartvo cartvo=new Cartvo();
        Connection connection=null;
        PreparedStatement preparedStatement=null;
        ResultSet resultSet=null;


return cartvo;

    }

}
