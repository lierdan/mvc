package com.icss.restaurant.service.impl;

import com.icss.restaurant.dao.MenuDao;
import com.icss.restaurant.dao.impl.MenuDaoImpl;
import com.icss.restaurant.entity.Menu;
import com.icss.restaurant.service.MenuService;
import com.icss.restaurant.util.DBFactory;
import com.icss.restaurant.util.PageUtil;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class MenuServiceImpl implements MenuService {
    private MenuDao menuDao=new MenuDaoImpl();
    @Override
    public int addMenu(Menu menu) {
        int row=0;
        row=menuDao.addMenu(menu);
        try {
            DBFactory.closeConnection();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return row;
    }

    @Override
    public int deleteMenu(int mid) {
        int row=0;
        row=menuDao.deleteMenu(mid);
        try {
            DBFactory.closeConnection();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return row;
    }

    @Override
    public int updateMenu(Menu menu) {
        int row=0;
        row=menuDao.updateMenu(menu);
        try {
            DBFactory.closeConnection();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return row;
    }

    @Override
    public List<Menu> findAllMenu() {
        List<Menu> list=new ArrayList<>();
        list=menuDao.findAllMenu();
        try {
            DBFactory.closeConnection();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    @Override
    public int findMenuCount() throws Exception {
        int row=0;
        Connection connection=DBFactory.openConnection();
        String sql="select count(*) from menu";
        PreparedStatement preparedStatement=connection.prepareStatement(sql);
        ResultSet resultSet=preparedStatement.executeQuery();
        if (resultSet.first()) {
            row=resultSet.getInt(1);
        }
        return row;
    }

    @Override
    public PageUtil<Menu> findAllMenuByPage(int currentPage, int pageSize) throws Exception {
        PageUtil<Menu>pageUtil=new PageUtil<>();

        List<Menu>list=menuDao.findAllMenu();
        int total=menuDao.findMenuCount();
        int pageCount=total%pageSize==0?total/pageSize:total/pageSize+1;
        pageUtil.setCurrentPage(currentPage);
        pageUtil.setList(list);
        pageUtil.setCurrentPage(pageCount);
        pageUtil.setPageSize(pageSize);
        pageUtil.setTatal(total);
        DBFactory.closeConnection();
        return pageUtil;
    }

    @Override
    public Menu findById(int mid) throws Exception {
        Menu menu=null;
        try {
            menu= menuDao.findById(mid);
        } catch (Exception e) {
            e.printStackTrace();
        }finally {
            DBFactory.closeConnection();
        }

        return menu;
    }

    public static void main(String[] args) throws Exception {
        System.out.println(new MenuServiceImpl().findById(1));
    }
}
