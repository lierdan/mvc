package com.icss.restaurant.service.impl;

import com.icss.restaurant.entity.User;
import com.icss.restaurant.service.UserService;
import com.icss.restaurant.util.DBFactory;
import org.junit.Test;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class UserServiceImpl implements UserService {
    @Override
    public User login(String uname, String upwd) {
        User user=null;

        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;

        try {
            connection = DBFactory.openConnection();
            String sql = "select *  from user where uname = ? and upwd= ?";
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, uname);
            preparedStatement.setString(2, upwd);
            resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                user=new User();
                user.setUid(resultSet.getInt("uid"));
                user.setUname(resultSet.getString("uname"));
                user.setUpwd(resultSet.getString("upwd"));
                user.setUsex(resultSet.getInt("usex"));
                user.setUage(resultSet.getInt("uage"));
                user.setUaddress(resultSet.getString("uaddress"));
                user.setUphone(resultSet.getString("uphone"));
                user.setUemail(resultSet.getString("uemail"));
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (preparedStatement != null) {
                try {
                    preparedStatement.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            try {
                DBFactory.closeConnection();
            } catch (SQLException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }


        return user;
    }



    /**
     *     uname, upwd, usex, uage, uaddress, uphone
     * @param user
     * @return
     */
    @Override
    public int register(User user) {
        String uname=user.getUname();
        String upwd=user.getUpwd();
        int usex=user.getUsex();
        int uage=user.getUage();
        String uaddress=user.getUaddress();
        String uphone=user.getUphone();
        String uemail=user.getUemail();
        int row = 0;
        Connection connection=null;
        PreparedStatement preparedStatement=null;

        try {
            connection=DBFactory.openConnection();
            String sql="insert into user (uname, upwd, usex, uage, uaddress, uphone,uemail) values (?,?,?,?,?,?,?)";
            preparedStatement=connection.prepareStatement(sql);
//            uname, upwd, usex, uage, uaddress, uphone
            preparedStatement.setString(1,uname);
            preparedStatement.setString(2,upwd);
            preparedStatement.setInt(3,usex);
            preparedStatement.setInt(4,uage);
            preparedStatement.setString(5,uaddress);
            preparedStatement.setString(6,uphone);
            preparedStatement.setString(7,uemail);
            row=preparedStatement.executeUpdate();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return row;
    }
}
