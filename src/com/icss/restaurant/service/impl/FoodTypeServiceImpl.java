package com.icss.restaurant.service.impl;

import com.icss.restaurant.dao.FoodTypeDao;
import com.icss.restaurant.dao.impl.FoodTypeDaoImpl;
import com.icss.restaurant.entity.FoodType;
import com.icss.restaurant.service.FoodTypeService;
import com.icss.restaurant.util.DBFactory;
import org.junit.Test;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class FoodTypeServiceImpl implements FoodTypeService {
    private FoodTypeDao foodTypeDao = new FoodTypeDaoImpl();

    @Override
    public int addFoodType(String foodTypeName) {
        int row = 0;
        row = foodTypeDao.addFoodType(foodTypeName);
        try {
            DBFactory.closeConnection();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return row;
    }



    @Override
    public int deleteFoodType(int ftno) {
        int row = 0;
        row = foodTypeDao.deleteFoodType(ftno);
        try {
            DBFactory.closeConnection();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return row;
    }

    @Override
    public int updateFoodTypeName(FoodType foodType) {
        int row = 0;
        row = foodTypeDao.updateFoodTypeName(foodType);
        try {
            DBFactory.closeConnection();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return row;
    }

    @Override
    public List<FoodType> findAllFoodType() {
        List<FoodType> list=new ArrayList<>();
        list=foodTypeDao.findAllFoodType();
        return list;
    }
}
