package com.icss.restaurant.service;

import com.icss.restaurant.entity.User;

public interface UserService {
    public User login(String uname ,String upwd);
    public int register(User user);
}
