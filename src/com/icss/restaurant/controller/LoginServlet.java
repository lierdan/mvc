package com.icss.restaurant.controller;

import com.icss.restaurant.entity.User;
import com.icss.restaurant.service.UserService;
import com.icss.restaurant.service.impl.UserServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "LoginServlet")
public class LoginServlet extends HttpServlet {
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //1、接受页面提交得参数
        String uname = request.getParameter("uname");
        String upwd = request.getParameter("upwd");
//        2\调用业务层方法
        UserService userService = new UserServiceImpl();
        User user=null;
        try {
            user = userService.login(uname, upwd);

        } catch (Exception e) {
            e.printStackTrace();
            request.getRequestDispatcher("error.html").forward(request,response);
        }

        if (user != null) {
            request.getRequestDispatcher("index.jsp").forward(request, response);
        } else {
//            request.getRequestDispatcher("error.html").forward(request,response);;
            response.setContentType("text/html;charset=utf-8");
            PrintWriter printWriter=response.getWriter();
            request.setAttribute("user",user);
            request.getRequestDispatcher("login.jsp").forward(request, response);

        }

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
