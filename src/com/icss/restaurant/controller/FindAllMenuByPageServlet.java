package com.icss.restaurant.controller;

import com.icss.restaurant.dao.MenuDao;
import com.icss.restaurant.dao.impl.MenuDaoImpl;
import com.icss.restaurant.entity.Menu;
import com.icss.restaurant.util.PageUtil;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "FindAllMenuByPageServlet")
public class FindAllMenuByPageServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String page=request.getParameter("currentPage");
        int currentPage=1;
        if (page!=null&& !"".equals(page)) {
            currentPage=Integer.parseInt(page);
        }
        int pageSize=5;


        MenuDao menuDao=new MenuDaoImpl();
        PageUtil<Menu>pageUtil=new PageUtil<>();
        try {
            pageUtil= (PageUtil<Menu>) menuDao.findAllMenuByPage(currentPage,pageSize);
        } catch (Exception e) {
            e.printStackTrace();
        }

        request.setAttribute("pageUtil",pageUtil);
        request.getRequestDispatcher("back/foodinfoList.jsp").forward(request,response);

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request,response);
    }
}
