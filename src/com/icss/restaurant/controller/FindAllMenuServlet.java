package com.icss.restaurant.controller;

import com.icss.restaurant.entity.Menu;
import com.icss.restaurant.service.MenuService;
import com.icss.restaurant.service.impl.MenuServiceImpl;
import org.junit.Test;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

@WebServlet(name = "FindAllMenuServlet")
public class FindAllMenuServlet extends HttpServlet {
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request,response);
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        List <Menu> listMenu=new ArrayList<Menu>();
        MenuService menuService=new MenuServiceImpl();
        try {
            listMenu=menuService.findAllMenu();
        } catch (Exception e) {
            request.getRequestDispatcher("error.jsp").forward(request, response);
            return ;
        }
        request.setAttribute("list",listMenu);
        request.getRequestDispatcher("index.jsp").forward(request, response);
    }
}
