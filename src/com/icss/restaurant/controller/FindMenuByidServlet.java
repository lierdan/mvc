package com.icss.restaurant.controller;

import com.icss.restaurant.entity.Menu;
import com.icss.restaurant.service.MenuService;
import com.icss.restaurant.service.impl.MenuServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "FindMenuByidServlet", urlPatterns = "/FindMenuByidServlet")
public class FindMenuByidServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//        1.接受参数
        String id = request.getParameter("mid");
        int mid = 0;
        if (id != null && !"".equals(id)) {
            mid = Integer.parseInt(id);
        }
        MenuService menuService = new MenuServiceImpl();
        Menu menu = null;
        try {
            menu = menuService.findById(mid);
        } catch (Exception e) {
            e.printStackTrace();
        }
        request.setAttribute("menu", menu);
        request.getRequestDispatcher("detail.jsp").forward(request, response);


    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }
}
