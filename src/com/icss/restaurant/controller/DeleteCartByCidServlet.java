package com.icss.restaurant.controller;

import com.icss.restaurant.entity.Cart;
import com.icss.restaurant.service.cartService;
import com.icss.restaurant.service.impl.cartServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "DeleteCartByCidServlet")
public class DeleteCartByCidServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //接收参数
        String cid=request.getParameter("cid");
        //2.调用业务层代码
        cartService service=new cartServiceImpl();
        try {
            service.deleteCart(Integer.parseInt(cid));

        } catch (Exception e) {
            e.printStackTrace();
        }
        //3、跳转
        request.getRequestDispatcher("FindAllByUidServlet").forward(request,response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
