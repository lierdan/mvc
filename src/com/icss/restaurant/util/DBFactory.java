package com.icss.restaurant.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBFactory {
    private  static ThreadLocal<Connection> tl=new InheritableThreadLocal<>();


    public static Connection openConnection() throws Exception {
        DBInfo dBInfo = DBInfo.getInstance();
        Connection connection=tl.get();
        if (connection==null) {
            //加载驱动
            Class.forName(dBInfo.getDriverClass());
            //建立驱动
            connection = DriverManager.getConnection(dBInfo.getUrl(), dBInfo.getUsername(), dBInfo.getUserpwd());
            tl.set(connection);
        }
        return connection;
    }

    public static void closeConnection() throws Exception {
        Connection connection=tl.get();
        if (connection != null) {
            connection.close();
            tl.remove();
        }
    }

    /**
     * 开启事务手动控制
     * @throws SQLException
     */
    public static void beginTransaction() throws SQLException {
        Connection connection=tl.get();
        if (connection!=null) {
            connection.setAutoCommit(false);
        }
    }

    /**
     * 事务回滚
     * @throws SQLException
     */
    public  static void rollback() throws SQLException {{
        Connection connection=tl.get();
        if (connection!=null) {
            connection.rollback();
        }
        }
    }

    /**
     * 手动提交
     * @throws SQLException
     */
    public static void commit() throws SQLException {
        Connection connection=tl.get();
        if (connection!=null) {
            connection.commit();
        }
    }


}
