package com.icss.restaurant.util;

import com.sun.corba.se.impl.orb.ParserTable;
import org.junit.Test;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URLDecoder;
import java.util.Properties;

public class DBInfo {
    private static DBInfo dBInfo;
    private String driverClass;
    private String url;
    private String username;
    private String userpwd;

    public String getDriverClass() {
        return driverClass;
    }

    public String getUrl() {
        return url;
    }

    public String getUsername() {
        return username;
    }

    public String getUserpwd() {
        return userpwd;
    }

    private DBInfo() {
        Properties properties = new Properties();
        try {
            String path=DBInfo.class.getResource("/").getPath()+"DB.properties";
            properties.load(new FileInputStream(new File(path)));
//            E:\\code\\workspace\\ideproject\\j2eestudy\\src\\com\\icss\\restaurant\\util\\DB.properties
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        this.driverClass = properties.getProperty("driverClass");
        this.url = properties.getProperty("url");
        this.username = properties.getProperty("username");
        this.userpwd = properties.getProperty("userpwd");
    }

    public static DBInfo getInstance() {
        if (dBInfo == null) {
            dBInfo = new DBInfo();
        }
        return dBInfo;
    }



}
