<%@ page import="java.util.Date" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="f" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>el</title>

  </head>
  
  <body>
   1+1=:${1+1}<br>
   1==2:${1==2}<br>
   1 eq 1=:${1 eq 1}<br>
   true && false=:${true && false}<br>
   <%
    pageContext.setAttribute("name","pageContext");
    /*  pageContext.setAttribute("name--","pageContext",p); */
    request.setAttribute("name","request");
    session.setAttribute("name","session");
    application.setAttribute("name","application");
    %>
    ${pageScore.name}<!--不指定就从小到大的查找-->
    ${requestScore.name}
    ${sessionScore.name}
    ${applictationScore.name}
<br>
<%--  条件判断--%>
<c:set var="mc" value="2">
    <c:when test="${mc==1}">
        1
    </c:when>
    <c:when test="${mc==2}">
        2
    </c:when>
    <c:otherwise>
        ?
    </c:otherwise>
</c:set>
   <br>
   <%
       Date date=new Date();
       pageContext.setAttribute("date",date);
   %>
   <fmt:formatDate value="${date}" pattern="yyyy-MM-dd hh:mm:ss"></fmt:formatDate>
<br>
  </body>
</html>
