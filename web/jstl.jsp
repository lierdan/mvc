<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@ page import="com.icss.restaurant.entity.Cart" %>
<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>jstl</title>
  </head>
  
  <body>
  <!--通用  -->
    <c:set var="salary" value="${1000*10 }" scope="request"> </c:set>
    <c:out value="${salary }" default="2500"></c:out>
    <c:remove var="salary" scope="session"/>
  <!--条件判断-->
  <c:if test="${salary>5000}">有钱人</c:if>
  <c:if test="${salary<5000}">没钱人</c:if>
  <!--循环迭代-->
  <%
   List<Cart> list=new ArrayList<Cart>();
   for(int i=0;i<5;i++){
   Cart cart=new Cart();
   list.add(cart);
   }
   request.setAttribute("list2", list);
   %>
   <c:forEach var="ca" items="${requestScope.list2}" step="1" begin="0" end="2" varStatus="status">
   ${ca}--${status.index}--${status.count}
   </c:forEach>
  </body>
</html>
